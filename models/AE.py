#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@Author ：hhx
@Date ：2022/5/19 21:33 
@Description ：最简单的自编码器
"""

import torch
import torch.nn as nn
import torch.nn.functional as F


class Autoencoder(nn.Module):
    def __init__(self):
        super(Autoencoder, self).__init__()
        # encoder
        self.enc1 = nn.Conv2d(in_channels=3, out_channels=16, kernel_size=3)
        self.enc2 = nn.Conv2d(in_channels=16, out_channels=32, kernel_size=3)
        self.enc3 = nn.Conv2d(in_channels=32, out_channels=64, kernel_size=3)
        # decoder
        self.dec1 = nn.ConvTranspose2d(in_channels=64, out_channels=32, kernel_size=3)
        self.dec2 = nn.ConvTranspose2d(in_channels=32, out_channels=16, kernel_size=3)
        self.dec3 = nn.ConvTranspose2d(in_channels=16, out_channels=3, kernel_size=3)

    def forward(self, x):
        self.x1 = F.relu(self.enc1(x))
        self.x2 = F.relu(self.enc2(self.x1))
        self.x3 = F.relu(self.enc3(self.x2))
        self.x4 = F.relu(self.dec1(self.x3))
        self.x5 = F.relu(self.dec2(self.x4))
        x = F.relu(self.dec3(self.x5))
        return x
